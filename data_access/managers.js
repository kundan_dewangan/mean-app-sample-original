(function() {

    //Get data
        function getManagersList(query, options, helpers, cb) {
        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Managers = dbClient.collection('managers');
                Managers.find(query).toArray(function(retrievalErr, managers) {                    
                    if (!retrievalErr) {
                        helpers.execute(cb, [managers]);
                        //console.log(query);
                    } else {
                        helpers.execute(cb, [retrievalErr, null]);
                    }
                });
            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    //Registration Process
    function createManagers(query, options, helpers, cb) {
        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Managers = dbClient.collection('managers');
                Managers.insertOne(query,function(retrievalErr, managers) {                    
                    if (!retrievalErr) {
                        helpers.execute(cb, [query, managers]);
                    } else {
                        helpers.execute(cb, [retrievalErr, null]);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    function updateManagers(query, options, helpers, cb) {
        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Managers = dbClient.collection('managers');
                Managers.findOneAndUpdate(query,options,function(retrievalErr, managers) {                    
                    if (!retrievalErr) {
                        helpers.execute(cb, [query, managers]);
                    } else {
                        helpers.execute(cb, [retrievalErr, null]);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    exports.Managers = {
        updateManagers:updateManagers,
        getManagersList: getManagersList, 
        createManagers:createManagers
    }

})();