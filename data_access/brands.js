(function() {
    function createBrands(query, options, helpers, cb, brand_name) {
        console.log(query);
        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Brands = dbClient.collection('brands');
                Brands.find({ brand_name: query.brand_name }).toArray().then((result) => {
                    if (result.length !== 0) {
                        helpers.execute(cb, ['already Brand exist']);
                    } else {
                        Brands.insert(query, function(retrievalErr, brands) {
                            if (!retrievalErr) {
                                helpers.execute(cb, [query, brands]);
                            } else {
                                helpers.execute(cb, [retrievalErr, null]);
                            }
                        });

                    }
                });
            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    function getBrands(query, options, helpers, cb) {
        console.log(query);
        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Brands = dbClient.collection('brands');
                Brands.find(query).toArray(function(retrievalErr, brands) {
                    if (!retrievalErr) {
                        helpers.execute(cb, [brands]);
                    } else {
                        helpers.execute(cb, [retrievalErr, null]);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    exports.Brands = {
        createBrands: createBrands,
        getBrands: getBrands
    }

})();