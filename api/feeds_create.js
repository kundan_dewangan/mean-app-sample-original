(function() {

    var Feeds = require('../data_access/feeds').Feeds;
    var brand = require('../data_access/brands').Brands;

    function execute(rqst, res, next, helpers) {
        var date = new Date();
        var user_name = rqst.body.user_name;
        var phon_no = rqst.body.phon_no;
        var email = rqst.body.email;
        var rating = rqst.body.rating;
        var brand_name = rqst.body.brand_name.trim();
        var date = date.toISOString();

        if (rating !== "") {
            brand.getBrands({ brand_name: brand_name }, {}, helpers, function(brands) {
                var NameToid = brands[0]._id;
                brand_id = NameToid;
                Feeds.createFeeds({ user_name, phon_no, email, rating, brand_id, date }, {}, helpers, function(response) {
                    res.json(response)
                })
            })
        } else {
            console.log("please rating fill the must");
        }
    }

    function getdata(rqst, res, next, helpers) {
        Feeds.getFeeds({}, {}, helpers, function(response) {
            res.json(response)
        })

    }
    exports.getdata = getdata;
    exports.execute = execute;

})()